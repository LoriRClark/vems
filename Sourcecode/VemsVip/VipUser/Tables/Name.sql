﻿CREATE TABLE [VipUserService].[Name]
(
	[NameID] INT           IDENTITY (1, 1) NOT NULL, 
    [NameFirst] NVARCHAR(MAX) NOT NULL, 
    [NameMiddle] NVARCHAR(MAX) NOT NULL, 
    [NameLast] NVARCHAR(MAX) NOT NULL, 
    [NamePrefix] NVARCHAR(MAX) NOT NULL, 
    [NameSuffix] NVARCHAR(MAX) NOT NULL,
	[IsPrimary] BIT NOT NULL, 
    [VIPUserID] INT NOT NULL,
	[DateOfVerification] DATETIME NOT NULL, 
    [IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

    PRIMARY KEY CLUSTERED ([NameID] ASC),
	FOREIGN KEY ([VIPUserID]) REFERENCES [VipUserService].[VIPUser] ([VIPUserID]) ,

)
