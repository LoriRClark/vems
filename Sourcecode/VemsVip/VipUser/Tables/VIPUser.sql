﻿CREATE TABLE [VipUserService].[VIPUser] (
    [VIPUserID]   INT           IDENTITY (1, 1) NOT NULL,
    [VIPUserName] NVARCHAR(MAX) NOT NULL,
    [UserType] NVARCHAR(MAX) NOT NULL, 
    [DateOfBirth] DATE  NULL, 
    [SSN] NVARCHAR(MAX) NULL, 
	[ContactInformationID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([VIPUserID] ASC),
	FOREIGN KEY ([ContactInformationID]) REFERENCES [ContactService].[ContactInformation] ([ContactInformationID]) ON UPDATE CASCADE ON DELETE NO ACTION,
);

