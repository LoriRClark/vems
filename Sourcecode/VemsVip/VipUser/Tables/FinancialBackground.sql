﻿CREATE TABLE [VipUserService].[FinancialBackground]
(
	[FinancialBackgroundID] INT           IDENTITY (1, 1) NOT NULL, 
    [BankruptcyCharges] NVARCHAR(MAX)  NULL, 
    [CriminalFelonyCharges] NVARCHAR(MAX)  NULL, 
    [FederalLoanDefault] NVARCHAR(MAX)  NULL, 
    [JudgementsFederalLiens] NVARCHAR(MAX)  NULL, 
    [VIPUserID] INT NOT NULL,
	[DateOfVerification] DATETIME  NULL, 
	[IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX)  NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([FinancialBackgroundID] ASC),
	FOREIGN KEY ([VIPUserID]) REFERENCES [VipUserService].[VIPUser] ([VIPUserID]) ON UPDATE CASCADE ON DELETE NO ACTION,

)
