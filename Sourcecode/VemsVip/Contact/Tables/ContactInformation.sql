﻿CREATE TABLE [ContactService].[ContactInformation] (
    [ContactInformationID] INT           IDENTITY (1, 1) NOT NULL,
    [PersonalPhoto]            IMAGE         NOT NULL,
    [PreferredMethod]         NVARCHAR (MAX) NOT NULL,
	[Website]                 NVARCHAR (MAX) NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([ContactInformationID] ASC)
    
);

