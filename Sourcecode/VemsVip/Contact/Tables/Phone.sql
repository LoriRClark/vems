﻿CREATE TABLE [ContactService].[Phone]
(
	[PhoneID] INT           IDENTITY (1, 1) NOT NULL, 
    [PhoneNumber] NVARCHAR(MAX) NOT NULL, 
    [Extension] NVARCHAR(MAX) NULL, 
    [PhoneType] NVARCHAR(MAX) NOT NULL, 
    [ContactInformationID] INT NOT NULL,
	[DateOfVerification] DATETIME NOT NULL, 
    [IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([PhoneID] ASC),
	FOREIGN KEY ([ContactInformationID]) REFERENCES [ContactService].[ContactInformation] ([ContactInformationID]) ON UPDATE CASCADE ON DELETE NO ACTION

)
