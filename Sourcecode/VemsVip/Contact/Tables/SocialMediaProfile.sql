﻿CREATE TABLE [ContactService].[SocialMediaProfile] (
    [SocialMediaProfileID]   INT           IDENTITY (1, 1) NOT NULL,
    [ProfileAddress]        NVARCHAR (MAX) NOT NULL,
    [SocialMediaProfileType] NVARCHAR (MAX) NOT NULL,
    [ContactInformationID]              INT           NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0,
    PRIMARY KEY CLUSTERED ([SocialMediaProfileID] ASC),
    FOREIGN KEY ([ContactInformationID]) REFERENCES [ContactService].[ContactInformation] ([ContactInformationID]) ON UPDATE CASCADE ON DELETE NO ACTION
);

