﻿CREATE TABLE [ContactService].[Address] (
    [AddressID]    INT           IDENTITY (1, 1) NOT NULL,
    [AddressLine1] NVARCHAR (MAX) NOT NULL,
    [AddressLine2] NVARCHAR (MAX) NULL,
    [AddressLine3] NVARCHAR (MAX) NULL,
	[AddressType]  NVARCHAR (MAX) NOT NULL,
    [City]        NVARCHAR (MAX) NOT NULL,
    [County]      NVARCHAR (MAX) NOT NULL,
    [State]       NVARCHAR (MAX) NOT NULL,
    [Zip]         NVARCHAR (MAX) NOT NULL,
    [ZipPlusFour] NVARCHAR (MAX) NULL,
    [ContactInformationID] INT NOT NULL, 
	[DateOfVerification] DATETIME NOT NULL, 
    [IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

    PRIMARY KEY CLUSTERED ([AddressID] ASC),
    FOREIGN KEY ([ContactInformationID]) REFERENCES [ContactService].[ContactInformation] ([ContactInformationID]) ON UPDATE CASCADE ON DELETE NO ACTION
);

 