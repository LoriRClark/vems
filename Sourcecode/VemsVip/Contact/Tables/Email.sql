﻿CREATE TABLE [ContactService].[Email]
(
	[EmailID] INT           IDENTITY (1, 1) NOT NULL, 
    [Email] NVARCHAR(MAX) NOT NULL, 
    [EmailType] NVARCHAR(MAX) NOT NULL,
	[ContactInformationID] INT NOT NULL,
	[DateOfVerification] DATETIME NOT NULL, 
    [IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([EmailID] ASC),
	FOREIGN KEY ([ContactInformationID]) REFERENCES [ContactService].[ContactInformation] ([ContactInformationID]) ON UPDATE CASCADE ON DELETE NO ACTION

)
