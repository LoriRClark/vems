﻿CREATE TABLE [VipUserService].[CVECertification]
(
	[CVECertificationID] INT           IDENTITY (1, 1) NOT NULL, 
    [CVECertificationType] NVARCHAR(MAX) NOT NULL, 
    [DateOfCertification] DATETIME NOT NULL, 
    [DateOfExpiration] DATETIME NOT NULL, 
    [IsRenewal] BIT NOT NULL, 
    [BusinessID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([CVECertificationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
