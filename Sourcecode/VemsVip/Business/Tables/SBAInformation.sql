﻿CREATE TABLE [VipUserService].[SBAInformation]
(
	[SBAInformationID] INT           IDENTITY (1, 1) NOT NULL, 
    [NAICSCodes] NVARCHAR(MAX) NOT NULL, 
    [SBACertifications] NVARCHAR(MAX) NOT NULL, 
    [BusinessID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([SBAInformationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
