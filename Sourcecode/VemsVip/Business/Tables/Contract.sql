﻿CREATE TABLE [VipUserService].[Contract]
(
	[ContractID] INT           IDENTITY (1, 1) NOT NULL,
    [ContractingOfficerName] NVARCHAR(MAX) NOT NULL, 
    [ContractingOfficerVIPUserID] INT NOT NULL, 
    [ContractNumber] NVARCHAR(MAX) NOT NULL, 
    [ContractType] NVARCHAR(MAX) NOT NULL, 
    [ContractValue] INT NOT NULL, 
    [IssuingAgency] NVARCHAR(MAX) NOT NULL, 
    [IssuingOrganizationName] NVARCHAR(MAX) NOT NULL, 
    [IssuingOrganizationPhoneNumber] NVARCHAR(MAX) NOT NULL, 
    [BusinessID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([ContractID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION,
)
