﻿CREATE TABLE [VipUserService].[FederalContractingInformation]
(
	[FederalContractingInformationID] INT           IDENTITY (1, 1) NOT NULL, 
    [CAGECode] INT NULL, 
    [BusinessID] INT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([FederalContractingInformationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
