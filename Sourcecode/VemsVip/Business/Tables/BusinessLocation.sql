﻿CREATE TABLE [VipUserService].[BusinessLocation]
(
    [BusinessLocationID] INT           IDENTITY (1, 1) NOT NULL,
    [DUNSNumber] NVARCHAR(MAX) NOT NULL,
	[SecurityClearance] NVARCHAR(MAX) NOT NULL, 
    [BusinessID] INT NOT NULL, 
    [BusinessContactID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([BusinessLocationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY ([BusinessContactID]) REFERENCES [VipUserService].[BusinessContactInformation] ([BusinessContactID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
