﻿CREATE TABLE [VipUserService].[BusinessOwner]
(
	[BusinessOwnerID] INT           IDENTITY (1, 1) NOT NULL, 
    [BusinessTitle] NVARCHAR(MAX) NULL, 
    [OwnershipPercent] INT NULL, 
    [BusinessID] INT NULL, 
    [VIPUserID] INT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([BusinessOwnerID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY ([VIPUserID]) REFERENCES [VipUserService].[VIPUser] ([VIPUserID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
