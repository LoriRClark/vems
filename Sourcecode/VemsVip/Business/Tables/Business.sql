﻿CREATE TABLE [VipUserService].[Business]
(
    [BusinessID] INT           IDENTITY (1, 1) NOT NULL,
    [CongressionalDistrict]NVARCHAR(MAX) NOT NULL,
	[NumberOfOperatingLocations] INT NOT NULL,
	[BusinessContactID] INT   NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([BusinessID] ASC),
	FOREIGN KEY ([BusinessContactID]) REFERENCES [VipUserService].[BusinessContactInformation] ([BusinessContactID])

)