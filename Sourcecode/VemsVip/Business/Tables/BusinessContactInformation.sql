﻿CREATE TABLE [VipUserService].[BusinessContactInformation]
(
	[BusinessContactID] INT           IDENTITY (1, 1) NOT NULL,
    [AddressCity] NVARCHAR(MAX) NOT NULL,
	[AddressCounty] NVARCHAR(MAX) NOT NULL, 
    [AddressLine1] NVARCHAR(MAX) NOT NULL, 
    [AddressLine2] NVARCHAR(MAX) NULL, 
    [AddressLine3] NVARCHAR(MAX) NULL, 
    [AddressState] NVARCHAR(MAX) NOT NULL, 
    [AddressZip] NVARCHAR(MAX) NOT NULL, 
    [AddressZipPlusFour] NVARCHAR(MAX) NULL, 
    [Email] NVARCHAR(MAX) NOT NULL, 
    [FaxNumber] NVARCHAR(MAX) NOT NULL, 
    [PhoneNumber] NVARCHAR(MAX) NOT NULL, 
    [PhoneNumberExtension] NVARCHAR(MAX) NULL, 
    [Website] NVARCHAR(MAX) NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([BusinessContactID] ASC)
)
