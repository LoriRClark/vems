﻿CREATE TABLE [VipUserService].[BusinessIdentification]
(
	[BusinessIdentificationID] INT           IDENTITY (1, 1) NOT NULL, 
    [BusinessName] NVARCHAR(MAX) NOT NULL, 
    [BusinessNameDBA] NVARCHAR(MAX) NOT NULL, 
    [BusinessOrganizationType] NVARCHAR(MAX) NOT NULL, 
    [BusinessType] NVARCHAR(MAX) NOT NULL, 
    [DUNSNumber] NVARCHAR(MAX) NOT NULL, 
    [EIN] NVARCHAR(MAX) NOT NULL, 
    [BusinessID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([BusinessIdentificationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION

)
