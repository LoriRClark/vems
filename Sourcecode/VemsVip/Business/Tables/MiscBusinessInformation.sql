﻿CREATE TABLE [VipUserService].[MiscBusinessInformation]
(
	[MiscBusinessInformationID] INT           IDENTITY (1, 1) NOT NULL, 
    [AnnualRevenue] INT NOT NULL, 
    [BondingLevelPerContract] NVARCHAR(MAX) NOT NULL,
    [CapabilityNarrative] NVARCHAR(MAX) NOT NULL,
    [LargestContractToDate] NVARCHAR(MAX) NOT NULL, 
    [NumberOfEmployees] INT NOT NULL, 
    [YearEstablished] NVARCHAR(MAX) NOT NULL, 
    [BusinessID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
	PRIMARY KEY CLUSTERED ([MiscBusinessInformationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
