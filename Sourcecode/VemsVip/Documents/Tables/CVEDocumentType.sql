﻿CREATE TABLE [VipUserService].[CVEDocumentType] (
    [CVEDocumentTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [CVEDocumentType]  NVARCHAR (MAX) NOT NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [DocumentTitle]    NVARCHAR (MAX) NOT NULL,
    [Rationale]        NVARCHAR (MAX) NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([CVEDocumentTypeID] ASC)
);

