﻿CREATE TABLE [VipUserService].[CVEDocument]
(
	[CVEDocumentID]          INT           IDENTITY (1, 1) NOT NULL,
	[Filename]           NVARCHAR (MAX) NOT NULL,
    [Description]        NVARCHAR (MAX) NULL,
	[DateEffective]      DATE      NOT NULL,
	[DateExpired]      DATE      NOT NULL,
    [CVEDocumentTypeID]   INT           NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([CVEDocumentID] ASC),
    FOREIGN KEY ([CVEDocumentTypeID]) REFERENCES [VipUserService].[CVEDocumentType] ([CVEDocumentTypeID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
