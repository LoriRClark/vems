﻿CREATE TABLE [VipUserService].[Recommendation]
(
	[RecommendationID] INT           IDENTITY (1, 1) NOT NULL, 
    [DateOfRecommendation] DATETIME NOT NULL, 
    [Phase] NVARCHAR(MAX) NOT NULL, 
    [Recommendation] NVARCHAR(MAX) NOT NULL, 
    [RecommendationBy] NVARCHAR(MAX) NOT NULL, 
    [VerifiedApplicationID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([RecommendationID] ASC),
	FOREIGN KEY ([VerifiedApplicationID]) REFERENCES [VipUserService].[Application] ([ApplicationID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
