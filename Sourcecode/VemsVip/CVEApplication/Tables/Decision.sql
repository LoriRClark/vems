﻿CREATE TABLE [VipUserService].[Decision]
(
	[DecisionID] INT           IDENTITY (1, 1) NOT NULL, 
    [DateOfDecision] DATETIME NOT NULL, 
    [Decision] NVARCHAR(MAX) NOT NULL, 
    [DeterminedBy] INT NOT NULL, 
    [DecisionLetterVerificationProcessLetterID] INT NOT NULL, 
    [RecommendationID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([DecisionID] ASC),
	FOREIGN KEY ([RecommendationID]) REFERENCES [VipUserService].[Recommendation] ([RecommendationID]) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY ([DecisionLetterVerificationProcessLetterID]) REFERENCES [VipUserService].[VerificationProcessLetter] ([VerificationProcessLetterID]) ON UPDATE CASCADE ON DELETE NO ACTION

)
