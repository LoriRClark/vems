﻿CREATE TABLE [VipUserService].[VerificationProcessLetter]
(
	[VerificationProcessLetterID] INT           IDENTITY (1, 1) NOT NULL,
    [VerificationLetterTemplateID] INT NOT NULL, 
    [VerificationProcessDocumentID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([VerificationProcessLetterID] ASC),
	FOREIGN KEY ([VerificationLetterTemplateID]) REFERENCES [VipUserService].[VerificationLetterTemplate] ([VerificationLetterTemplateID]) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY ([VerificationProcessDocumentID]) REFERENCES [VipUserService].[VerificationProcessDocument] ([VerificationProcessDocumentID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
