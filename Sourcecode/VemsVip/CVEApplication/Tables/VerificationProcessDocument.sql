﻿CREATE TABLE [VipUserService].[VerificationProcessDocument]
(
	[VerificationProcessDocumentID] INT           IDENTITY (1, 1) NOT NULL,
    [VerificationProcessDocumentType] NVARCHAR(MAX) NOT NULL, 
    [CreatedBy] NVARCHAR(MAX) NOT NULL, 
    [DateCreated] DATETIME NOT NULL, 
    [Description ] NVARCHAR(MAX) NOT NULL, 
    [Filename] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([VerificationProcessDocumentID] ASC)

)
