﻿CREATE TABLE [VipUserService].[VerificationLetterTemplate]
(
	[VerificationLetterTemplateID] INT           IDENTITY (1, 1) NOT NULL,
    [VerificationLetterType] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([VerificationLetterTemplateID] ASC)
	
)
