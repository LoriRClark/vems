﻿CREATE TABLE [VipUserService].[Application]
(
	[ApplicationID] INT           IDENTITY (1, 1) NOT NULL, 
    [ApplicationType] NVARCHAR(MAX) NOT NULL, 
    [CurrentStatus] NVARCHAR(MAX) NOT NULL, 
    [CurrentVerificationStatus] INT NOT NULL, 
    [DateCompleted] DATETIME NOT NULL, 
    [DateSubmitted] DATETIME NOT NULL, 
    [SensitivityLevel] NVARCHAR(MAX) NOT NULL, 
    [BusinessID] INT NOT NULL, 
    [VIPUserIDSubmitted] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([ApplicationID] ASC),
	FOREIGN KEY ([BusinessID]) REFERENCES [VipUserService].[Business] ([BusinessID]) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY ([VIPUserIDSubmitted]) REFERENCES [VipUserService].[VIPUser] ([VIPUserID]) ON UPDATE CASCADE ON DELETE NO ACTION
)
