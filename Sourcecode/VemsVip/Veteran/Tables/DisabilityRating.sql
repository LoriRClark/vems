﻿CREATE TABLE [VeteranService].[DisabilityRating] (
    [DisabilityRatingID] INT           IDENTITY (1, 1) NOT NULL,
    [DateOfRating]       DATE      NULL,
    [DisabilityRating]   INT           NULL,
    [RatingAgency]      NVARCHAR(MAX)  NULL,
    [VeteranID]          INT           NOT NULL,
	[DateOfVerification] DATETIME NOT NULL, 
    [IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

    PRIMARY KEY CLUSTERED ([DisabilityRatingID] ASC),
    FOREIGN KEY ([VeteranID]) REFERENCES [VeteranService].[Veteran] ([VeteranID]) ON UPDATE CASCADE ON DELETE NO ACTION
);

