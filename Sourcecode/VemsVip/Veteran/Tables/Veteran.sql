﻿CREATE TABLE [VeteranService].[Veteran] (
    [VeteranID]             INT           IDENTITY (1, 1) NOT NULL,
    [BranchOfService]      NVARCHAR (MAX) NOT NULL,
    [CharacterOfService]   NVARCHAR (MAX) NOT NULL,
    [DateDeceased]          DATE      NOT NULL,
    [DateEnteredActiveDuty] DATE      NOT NULL,
    [Rank]                  NVARCHAR (MAX) NOT NULL,
	[VAFileNumber]          NVARCHAR (MAX) NULL,
	[VIPUserID]              INT   NOT NULL, 
	[SpouseVIPUserID]	INT  NULL, 
	[DateOfVerification] DATE NOT NULL, 
    [IsVerified] BIT NOT NULL, 
    [VerificationSource] NVARCHAR(MAX) NOT NULL, 
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

    PRIMARY KEY CLUSTERED ([VeteranID] ASC),
	FOREIGN KEY ([VIPUserID]) REFERENCES [VipUserService].[VIPUser] ([VIPUserID]),
	FOREIGN KEY ([SpouseVIPUserID]) REFERENCES [VipUserService].[VIPUser] ([VIPUserID]),
);

