﻿CREATE TABLE [VeteranService].[VeteranSpouse]
(
	[VeteranSpouseID] INT           IDENTITY (1, 1) NOT NULL,
    [NameFirst] NVARCHAR(MAX) NOT NULL, 
    [NameMiddle] NVARCHAR(MAX) NOT NULL, 
    [NameLast] NVARCHAR(MAX) NOT NULL, 
	[DateOfBirth] NVARCHAR(MAX) NOT NULL,
    [SSN] NVARCHAR(MAX) NOT NULL,
	[SpouseVAFileNumber] NVARCHAR(MAX) NOT NULL, 
    [VeteranID] INT NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0, 

	PRIMARY KEY CLUSTERED ([VeteranSpouseID] ASC),
	FOREIGN KEY ([VeteranID]) REFERENCES [VeteranService].[Veteran] ([VeteranID]) ON UPDATE CASCADE ON DELETE NO ACTION,
)


