﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface IGetVIPUserByUsername_Request
    {
        String VIPUsername
         {
             get; 
             set; 
         }
    }

    public interface IGetVIPUserByUsername_Response
    {
        IVIPUser VIPUser
        {
            get;
            set;
        }
    }
}
