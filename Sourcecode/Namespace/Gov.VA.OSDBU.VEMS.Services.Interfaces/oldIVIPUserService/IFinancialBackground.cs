﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface IFinancialBackground
    {
        int FinancialBackgroundID
        {
            get;
            set;
        }

        int VIPUserID
        {
            get;
            set;
        }

        String FederalLoadDefault
        {
            get;
            set;
        }

        String CriminalFelonyCharges
        {
            get;
            set;
        }

        String JudgementFederalLiens
        {
            get;
            set;
        }

        String BankruptcyCharges
        {
            get;
            set;
        }
    }
}
