﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface IGetVIPUsersByType_Request
    {
        String UserType
         {
             get; 
             set; 
         }
    }

    public interface IGetVIPUsersByType_Response
    {
        List<IVIPUser> VIPUsers
        {
            get;
            set;
        }
    }
}
