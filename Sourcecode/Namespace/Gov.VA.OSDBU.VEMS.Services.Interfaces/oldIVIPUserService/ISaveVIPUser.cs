﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface ISaveVIPUser_Request
    {
        IVIPUser VIPUser
        {
            get;
            set;
        }
    }

    public interface ISaveVIPUser_Response
    {
        IVIPUser VIPUser
        {
            get;
            set;
        }
    }
}
