﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface IGetVIPUserFinancialBackgrounds_Request
    {
        int VIPUserID
         {
             get; 
             set; 
         }
    }

    public interface IGetVIPUserFinancialBackgrounds_Response
    {
        List<IFinancialBackground> FinancialBackgrounds
        {
            get;
            set;
        }
    }
}
