﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface IName
    {

        int NameID
        {
            get;
            set; 
        }

        int VIPUserID
        {
            get; 
            set; 
        }

        String NameFirst
        {
            get; 
            set; 
        }

        String NameLast
        {
            get; 
            set; 
        }

        String NameMiddle
        {
            get; 
            set; 
        }

        String NamePrefix
        {
            get; 
            set; 
        }

        String NameSuffix
        {
            get; 
            set; 
        }

    }
}
