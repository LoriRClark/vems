﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace = "http://Gov.VA.OSDBU.VEMS.Services.Interfaces")]
    public interface IVIPUserService
    {
        // Retrieve individual user
        [OperationContract]
        IGetVIPUser_Response GetVIPUser(IGetVIPUser_Request request);
        [OperationContract]
        IGetVIPUserByUsername_Response GetVIPUserByUsername(IGetVIPUserByUsername_Request request);

        // Retrieve multiple users
        [OperationContract]
        IGetVIPUsersByType_Response GetVIPUsersByType(IGetVIPUsersByType_Request request);

        // Retrieve user elements
        [OperationContract]
        IGetVIPUserFinancialBackgrounds_Response GetVIPUserFinancialBackgrounds(IGetVIPUserFinancialBackgrounds_Request request);
        [OperationContract]
        IGetVIPUserNames_Response GetVIPUserNames(IGetVIPUserNames_Request request);

        // Save user
        [OperationContract]
        ISaveVIPUser_Response SaveVIPUser(ISaveVIPUser_Request request);
        // save user elements
        [OperationContract]
        ISaveVIPUsername_Response SaveVIPUserName(ISaveVIPUsername_Request request);
        [OperationContract]
        ISaveVIPFinancialBackground_Response SaveVIPUserFinancialBackground(ISaveVIPFinancialBackground_Request request);

    }

}
