﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface ISaveVIPUsername_Request
    {
        IName Username
        {
            get;
            set;
        }
    }

    public interface ISaveVIPUsername_Response
    {
        IName Username
        {
            get;
            set;
        }
    }
}
