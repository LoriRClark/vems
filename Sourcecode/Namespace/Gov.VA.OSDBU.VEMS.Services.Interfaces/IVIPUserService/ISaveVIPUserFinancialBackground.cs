﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface ISaveVIPFinancialBackground_Request
    {
        IFinancialBackground FinancialBackground
        {
            get;
            set;
        }
    }

    public interface ISaveVIPFinancialBackground_Response
    {
        IFinancialBackground FinancialBackground
        {
            get;
            set;
        }
    }
}
