﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
    public interface IGetVIPUserNames_Request
    {
        int VIPUserID
         {
             get; 
             set; 
         }
    }

    public interface IGetVIPUserNames_Response
    {
        List<IName> Names
        {
            get;
            set;
        }
    }
}
