﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gov.VA.OSDBU.VEMS.Services.Interfaces.IVIPUserService
{
     public interface IVIPUser
    {
         int VIPUserID
         {
             get; 
             set; 
         }

         String Username
         {
             get; 
             set; 
         }

         Enum UserType
         {
             get; 
             set; 
         }

         String SSN 
         {
             get; 
             set; 
         }

         DateTime DateOfBirth
         {
             get; 
             set; 
         }

         String NameFirst
         {
             get; 
             set; 
         }

         String NameLast
         {
             get; 
             set; 
         }

         String NameMiddle
         {
             get; 
             set; 
         }

         String NamePrefix
         {
             get; 
             set; 
         }

         String NameSuffix
        {
            get; 
            set; 
        }

    }
}
